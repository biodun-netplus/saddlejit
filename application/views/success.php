<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Saddle</title>

    <!-- Bootstrap -->
    <link href="http://localhost/jitsaddle/saddleng/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://localhost/jitsaddle/saddleng/css/style.css" rel="stylesheet">
    <link href="http://localhost/jitsaddle/saddleng/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="http://localhost/jitsaddle/saddleng/css/form-elements.css">
    <link rel="stylesheet" href="http://localhost/jitsaddle/saddleng/css/style1.css">

    <link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">

        <div class="navbar-header">

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>

          <a class="navbar-brand" href="#">
            <div class="logo">
                <img src="img/logo.png"/>
            </div>
          </a>

        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#photo">SEND A PACKAGE</a></li>
                <li><a href="#photo">PRICING</a></li>
                <li><a href="#download">CONTACT US</a></li>
            </ul>
        </div>
      </div>
    </div>

    <style>
      strong {
        color: #F69147;
      }
    </style>

    <!-- Top content -->
    <section id="package">
        <div class="top-content">
            
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text" align="center">
                            <h1><strong>Thank you for using our service</strong> Package Sent Successfully</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 form-box">
                            
                        
                            <div class="form-top">
                                <div class="form-top-left">
                                    <h3>Step Completed</h3>
                                    <p>Package details:</p>
                                </div>
                            
                            
                                <div class="form-top-right">
                                    
                                </div>
                                <hr>
                                <br>
                               
                                <p align="center">
                                    <h3>Package Sent Successfull</h3>
                                    <br>
                                    <a href="http://localhost/jitsaddle/" class="btn btn-success" type="button">Done</a>
                                </p>
                                <br><br>
                            
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>


    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="http://localhost/jitsaddle/saddleng/js/bootstrap.min.js"></script>
    <script src="http://localhost/jitsaddle/saddleng/js/jquery.backstretch.min.js"></script>
    <script src="http://localhost/jitsaddle/saddleng/js/retina-1.1.0.min.js"></script>
    <script src="http://localhost/jitsaddle/saddleng/js/scripts.js"></script>
  </body>
</html>